﻿using NUnit.Framework;

namespace Page_Object_Model_Exercise2.Tests
{
    [TestFixture]
    public class SearchItem:Base
    {
        [Test]
        public void GivenAnExistingItemName_WhenSearching_ThenReturnItemCountGreaterThanZero()
        {
            StoreApp.MainPage.SearchInput.SendKeys("Dress");
            StoreApp.MainPage.SearchButton.Click();
            StoreApp.SearchPage.WaitForResults();

            Assert.IsTrue(StoreApp.SearchPage.SearchedItemNames.Count > 0);
        }
    }
}
