﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise2.PageObjectModel
{
    public interface IMainPage
    {
        IWebElement CenterColumn { get; }
        IWebElement SearchButton { get; }
        IWebElement SearchInput { get; }
    }
}