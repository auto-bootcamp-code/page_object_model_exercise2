﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace Page_Object_Model_Exercise2.PageObjectModel
{
    public interface ISearchPage
    {
        IList<IWebElement> SearchedItemNames { get; }

        void WaitForResults();
    }
}