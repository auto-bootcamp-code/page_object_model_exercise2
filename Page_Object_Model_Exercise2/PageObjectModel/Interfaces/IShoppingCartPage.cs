﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise2.PageObjectModel
{
    public interface IShoppingCartPage
    {
        IWebElement CartDeleteButton { get; }
        IWebElement CartEmptyAlert { get; }

        void WaitForCartAlert();
        void WaitForShoppingCartPage();
    }
}