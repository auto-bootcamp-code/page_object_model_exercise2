﻿using NUnit.Framework;

namespace Page_Object_Model_Exercise2.Tests
{
    [TestFixture]
    public class SendingEmail:Base
    {
        [SetUp]
        public new void Setup()
        {
            StoreApp.MainPage.SearchInput.SendKeys("Dress");
            StoreApp.MainPage.SearchButton.Click();
            StoreApp.SearchPage.WaitForResults();
            StoreApp.SearchPage.SearchedItemNames[0].Click();
            StoreApp.ProductPage.WaitForCartForm();
            StoreApp.ProductPage.SendFriendButton.Click();
            StoreApp.ProductPage.WaitForEmailForm();
        }

        [Test]
        public void GivenValidInput_WhenSendingAnEmail_ThenDisplaySuccessModal()
        {
            StoreApp.ProductPage.FriendName.SendKeys("KG");
            StoreApp.ProductPage.FriendEmail.SendKeys("kg@gmail.com");
            StoreApp.ProductPage.SendEmailButton.Click();
            StoreApp.ProductPage.WaitForEmailSentModal();

            Assert.IsTrue(StoreApp.ProductPage.EmailSentModal.Displayed);
        }

        [Test]
        public void GivenEmptyFields_WhenSendingAnEmail_ThenDisplayValidationMessage()
        {
            StoreApp.ProductPage.SendEmailButton.Click();
            StoreApp.ProductPage.WaitForErrorMessage();

            Assert.IsTrue(StoreApp.ProductPage.SendEmailError.Displayed);
        }
    }
}
