﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Page_Object_Model_Exercise2.PageObjectModel
{
    public interface IBasePage
    {
        IWebDriver Driver { get; set; }
        IWebElement PageRoot { get; }
        WebDriverWait Wait { get; set; }
    }
}