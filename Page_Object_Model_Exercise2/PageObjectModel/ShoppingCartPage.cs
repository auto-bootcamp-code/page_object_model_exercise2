﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise2.PageObjectModel
{
    public class ShoppingCartPage : IShoppingCartPage
    {
        private IBasePage _basePage;

        public ShoppingCartPage(IBasePage basePage)
        {
            _basePage = basePage;
        }

        private IWebElement shoppingCartPage => _basePage.Driver.FindElement(By.Id("order"));
        private IWebElement centerColumn => shoppingCartPage.FindElement(By.Id("center_column"));
        private IWebElement productRow => shoppingCartPage.FindElement(By.Id("product_5_19_0_0"));
        public IWebElement CartDeleteButton => productRow.FindElement(By.CssSelector("td.cart_delete a"));
        public IWebElement CartEmptyAlert => centerColumn.FindElement(By.CssSelector("p"));

        public void WaitForCartAlert()
        {
            _basePage.Wait.Until(driver => CartEmptyAlert.Displayed);
        }

        public void WaitForShoppingCartPage()
        {
            _basePage.Wait.Until(driver => shoppingCartPage.Displayed);
        }
    }
}
