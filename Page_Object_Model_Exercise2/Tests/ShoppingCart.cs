﻿using NUnit.Framework;

namespace Page_Object_Model_Exercise2.Tests
{
    [TestFixture]
    public class ShoppingCart:Base
    {
        [SetUp]
        public new void Setup()
        {
            StoreApp.MainPage.SearchInput.SendKeys("Dress");
            StoreApp.MainPage.SearchButton.Click();
            StoreApp.SearchPage.WaitForResults();
            StoreApp.SearchPage.SearchedItemNames[0].Click();
            StoreApp.ProductPage.WaitForCartForm();
            StoreApp.ProductPage.CartButton.Click();
            StoreApp.ProductPage.WaitForCartModal();
        }

        [Test]
        public void GivenAnExistingItemName_WhenAddingToCart_ThenDisplayCartModal()
        {
            Assert.IsTrue(StoreApp.ProductPage.CartModal.Displayed);
        }

        [Test]
        public void GivenAnExistingCartItem_WhenRemovingFromCart_ThenDisplayAlert()
        {
            StoreApp.ProductPage.CheckoutButton.Click();
            StoreApp.ShoppingCartPage.WaitForShoppingCartPage();
            StoreApp.ShoppingCartPage.CartDeleteButton.Click();
            StoreApp.ShoppingCartPage.WaitForCartAlert();

            Assert.IsTrue(StoreApp.ShoppingCartPage.CartEmptyAlert.Displayed);
        }
    }
}
